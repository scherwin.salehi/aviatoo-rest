<?php
namespace Aviatoo\Rest\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Aviatoo\Rest\Annotation\NotBlank;
use Aviatoo\Rest\Annotation\CustomGroups;
use JMS\Serializer\Annotation\Groups;

/**
 * Class CoordinateTrait
 * @package Aviatoo\Rest\Entity\Traits
 */
trait CoordinateTrait{

    /**
     * @Assert\NotBlank(groups={G::NEW})
     * @Groups({G::EDIT,G::NEW,G::ENTITY_OUT})
     * @ORM\Column(type="float")
     * @var $lat float
     */
    protected $lat;

    /**
     * @Assert\NotBlank(groups={G::NEW})
     * @Groups({G::EDIT,G::NEW,G::ENTITY_OUT})
     * @ORM\Column(type="float")
     * @var $lng float
     */
    protected $lng;

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat): void
    {
        $this->lat = $lat;
    }

}