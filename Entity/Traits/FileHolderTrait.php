<?php

namespace Aviatoo\Rest\Entity\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Aviatoo\Rest\Constants\GroupConstants;

trait FileHolderTrait
{
    /**
     * @Assert\NotBlank(groups={GroupConstants::UPLOAD})
     * @var UploadedFile
     */
    protected $file;

    /**
     * @return UploadedFile
     */
    public function getFile(){
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file): void
    {
        $this->file=$file;
    }
}