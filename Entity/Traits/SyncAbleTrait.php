<?php
namespace Aviatoo\Rest\Entity\Traits;

use Aviatoo\Rest\Entity\Interfaces\EntityInterface;
use Aviatoo\Rest\Exception\EntityNotFoundException;
use Aviatoo\Rest\Service\ParamConverter\SerializerService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use JMS\Serializer\Annotation\Groups;
use Aviatoo\Rest\Constants\GroupConstants;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;


trait SyncAbleTrait{

    protected $subEntities;
//    protected static $subCollections;

    public function getPrimaryKeys(){
        $class = get_class($this);
        return defined("self::converterPrimaryKey") ? self::converterPrimaryKey :"id";
    }
    public function getPrimaryValue(){
        $primaryKey = $this->getPrimaryKeys();
        $method = $this->_getter($primaryKey);
        return $this->$method();
    }


    public function getPrimaryFilter()
    {
        $res = [];
        $primaryKeys = $this->getPrimaryKeys();

        if (is_array($primaryKeys)) {
            foreach ($primaryKeys as $primaryKey) {
                $func = ($this->_getter($primaryKey));
                $value = $this->$func();
                if($value) $res[$primaryKey] = $value ;
                else return false;
            }
        } else if (is_string($primaryKeys)) {
            $value = $this->getPrimaryValue();
            if($value) $res[$primaryKeys] = $value;
            else return false;
        }

        return $res;
    }

    /**
     * @return array
     */
    protected static function getAttributeKeys(){
        return array_keys(get_class_vars(__CLASS__));
    }

    /**
     * @param $key
     * @return string
     */
    public function _getter($key){

        return "get".ucfirst($key);
    }

    /**
     * @param $key
     * @return string
     */
    public function _setter($key){
        return "set".ucfirst($key);
    }

    /**
     * @return bool
     */
    public function isInstanceOfThis(){
        $result=true;
        $args=func_get_args();
        if(sizeof($args)<1) return false;
        foreach ($args as $entity){
            if(get_class($this) !== get_class($entity)) $result=false;
        }
        return $result;
    }

    protected function keyIsSkippable($key){
        return $key === "subEntities";
    }

    protected function isPrimaryKey($key){
        $keys = $this->getPrimaryKeys();
        $isInKeys = is_array($keys) && is_numeric(array_search($key,$keys));
        $isKey = is_string($keys) && $keys === $key;
        return $isInKeys ||  $isKey;
    }

    /**
     * @param EntityInterface $defaultEntity
     * @return array
     */
    public function getChangedKeys(EntityInterface $defaultEntity){
        $result=[];
        if($this->isInstanceOfThis($defaultEntity)){
            foreach(self::getAttributeKeys() as $key){
                if($this->keyIsSkippable($key)) continue;
                $getter=$this->_getter($key);
                if( method_exists($this,$getter) && !$this->isPrimaryKey($key)&& $defaultEntity->$getter() !== $this->$getter())  $result[]=$key;
            }
        }
        return $result;
    }



    /**
     * @return array
     */
    public function getSubEntities(){

        if(!$this->subEntities){
            $this->subEntities = [];
            $keys = self::getAttributeKeys();

            foreach($keys as $key){
                if($this->keyIsSkippable($key)) continue;
                $getter=$this->_getter($key);

                if(method_exists($this,$getter)){
                    $value = $this->$getter();

                    if($value instanceof EntityInterface || $value instanceof ArrayCollection) $this->subEntities[$key] = $value;
//                    else if($value instanceof ArrayCollection) self::$subCollections[$key] = $value;
                }
            }
        }
        return $this->subEntities;
    }

//    private function fetchArray

    /**
     * @param SerializerService $serializerService
     * @param EntityInterface $entity
     * @return EntityInterface|object
     * @throws EntityNotFoundException
     */
    protected function subParamConversion(SerializerService $serializerService,EntityInterface $entity){
        $primaryFilter = $entity->getPrimaryFilter();

        if($primaryFilter){

            $exception = new EntityNotFoundException($entity);
            $entity = $serializerService->getEntityManager()->getRepository(get_class($entity))->findOneBy($primaryFilter);
            if(!$entity) throw $exception;
            return $entity;

        }else {
            $serializerService->validate($entity,["validate"=>true]);
            $subEntities = $entity->getSubEntities();
            forEach($subEntities as $key => $subEntity){
                $setter = $this->_setter($key);
                if(method_exists($entity,$setter)){
                    if($subEntity instanceof ArrayCollection){
                        $subEntity->map(function($entry) use($serializerService){
                            return $this->subParamConversion($serializerService,$entry);
                        });
                    }else $subEntity = $this->subParamConversion($serializerService, $subEntity);
                    $entity->$setter($subEntity);
                }
            }
        }
        return $entity;
    }

    /**
     * @param EntityInterface $newEntity
     * @param EntityInterface|null $defaultEntity
     * @param SerializerService $serializerService
     */
    public function sync(EntityInterface $newEntity,EntityInterface $defaultEntity=null, SerializerService $serializerService){

        if($defaultEntity===null){
            $class=get_class($defaultEntity);
            $defaultEntity=new $class();
        }

        if( $this->isInstanceOfThis($newEntity,$defaultEntity)){
            $changedKeys=$newEntity->getChangedKeys($defaultEntity);
            foreach ($changedKeys as $key){

                $setter=$this->_setter($key);
                $getter=$this->_getter($key);
                if(
                    method_exists($this ,$setter)&&
                    method_exists($newEntity ,$getter)&&
                    $this->$getter()!==$newEntity->$getter()
                ){
                    $value=$newEntity->$getter();

                    if($value instanceof EntityInterface){

                        $value = $this->subParamConversion($serializerService,$value);
                    } else if($value instanceof ArrayCollection){

                        $value = $value->map(function($entry) use($serializerService){
                            $res = $this->subParamConversion($serializerService,$entry);
                            return $res;
                        });
                    }

                    $this->$setter($value);
                }

            }

        }
    }
}