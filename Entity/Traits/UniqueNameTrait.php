<?php
namespace Aviatoo\Rest\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use Aviatoo\Rest\Constants\GroupConstants;

trait UniqueNameTrait{

    /**
     * @Assert\NotBlank(groups={GroupConstants::NEW})
     * @Assert\Length(
     *     min="4",
     *     max="15",
     *     groups={GroupConstants::NEW,GroupConstants::EDIT},
     * )
     * @Groups({GroupConstants::EDIT,GroupConstants::NEW,GroupConstants::ENTITY_OUT,GroupConstants::INDEX_OUT})
     * @ORM\Column(type="string",unique=true)
     */
    protected $name;

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name){
        $this->name=$name;
    }

}