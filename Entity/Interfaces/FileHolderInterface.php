<?php


namespace Aviatoo\Rest\Entity\Interfaces;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface FileHolderInterface
 * @package Aviatoo\Rest\Entity\Interfaces
 */
interface FileHolderInterface
{
    /**
     * @return File
     */
    public function getFile();

    /**
     * @param UploadedFile $file
     * @return void
     */
    public function setFile(UploadedFile $file): void;
}