<?php
namespace Aviatoo\Rest\Entity\Interfaces;

use Aviatoo\Rest\Entity\Article;
use Aviatoo\Rest\Entity\Company;
use Aviatoo\Rest\Entity\User;

/**
 * Interface OwnAbleInterface
 * @package Aviatoo\Rest\Entity\Interfaces
 */
interface OwnAbleInterface{
    /**
     * @param User $user
     * @return bool
     */
    public function isOwnedBy(User $user);

    /**
     * @return User
     */
    public function getOwner():User;
}
