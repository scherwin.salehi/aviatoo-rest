<?php
namespace Aviatoo\Rest\EventListener;

use Aviatoo\Rest\Exception\JWT\InvalidLoginException;
use Aviatoo\Rest\Exception\JWT\JWTInvalidException;
use Aviatoo\Rest\Exception\JWT\JWTExpiredException;
use Aviatoo\Rest\Exception\JWT\JWTNotFoundException;

/**
 * Class AuthenticationListener
 * @package Aviatoo\Rest\EventListener
 */
class AuthenticationListener
{
    /**
     * @throws InvalidLoginException
     */
    public function onAuthenticationFailureResponse()
    {
        throw new InvalidLoginException();
    }

    /**
     * @throws JWTInvalidException
     */
    public function onJWTInvalid()
    {
        throw new JWTInvalidException();
    }

    /**
     * @throws JWTNotFoundException
     */
    public function onJWTNotFound()
    {
        throw new JWTNotFoundException();
    }

    /**
     * @throws JWTExpiredException
     */
    public function onJWTExpired()
    {
        throw new JWTExpiredException();
    }
}
