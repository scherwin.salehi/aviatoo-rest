<?php
namespace Aviatoo\Rest\Service\ParamConverter;


use Aviatoo\Rest\Entity\Interfaces\EntityInterface;
use Aviatoo\Rest\Entity\Interfaces\OwnAbleInterface;
use Aviatoo\Rest\Entity\User;
use Aviatoo\Rest\Exception\AccessDeniedException;
use Aviatoo\Rest\Exception\EntityNotFoundException;
use Aviatoo\Rest\Exception\InvalidParamsException;
use Aviatoo\Rest\Service\User\UserService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Serializer\Serializer;
use JMS\Serializer\Exception\UnsupportedFormatException;
use Aviatoo\Rest\Exception\BadRequestHttpException;
use Aviatoo\Rest\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Serializer\Exception\ExceptionInterface as SymfonySerializerException;
use JMS\Serializer\Exception\Exception as JMSSerializerException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class SerializerService
 * @package Aviatoo\Rest\Service
 */
class SerializerService
{
    /**
     * @var Serializer
     */
    public $serializer,$validator,$validationErrorsArgument;
    private $entityManager,$userService;
    private $format="json",$class,$groups=[] ;
    private $object,$defaultEntity;

    /**
     * SerializerService constructor.
     * @param Serializer $serializer
     * @param ValidatorInterface|null $validator
     * @param string $validationErrorsArgument
     * @param EntityManagerInterface $entityManager
     * @param UserService $userService
     */
    public function __construct(Serializer $serializer,
                                ValidatorInterface $validator = null,
                                $validationErrorsArgument = "validationErrors",
                                EntityManagerInterface $entityManager,
                                UserService $userService)
    {

        if (null !== $validator && null === $validationErrorsArgument) {
            throw new \InvalidArgumentException('"$validationErrorsArgument" cannot be null when using the validator');
        }
        $this->validator=$validator;
        $this->validationErrorsArgument=$validationErrorsArgument;
        $this->serializer=$serializer;
        $this->entityManager=$entityManager;
        $this->userService=$userService;
    }

    /**
     * @param EntityInterface $entityA
     * @return object
     * @throws AccessDeniedException
     * @throws EntityNotFoundException
     */
    public function synchronizeEntity(EntityInterface $entityA){

        $this->class=get_class($entityA);
        $primaryFilter = $entityA->getPrimaryFilter();

        if($primaryFilter) {
            $entityB = $this->entityManager
                ->getRepository($this->class)
                ->findOneBy($primaryFilter);

            if(!$entityB) throw new EntityNotFoundException($entityA);
        }
        else $entityB = new $this->class();

        $defaultEntity = $this->getDefaultEntity();

        $entityB->sync($entityA,$defaultEntity,$this);

        if($entityB instanceof OwnAbleInterface){
            if(!$entityB->isOwnedBy($this->getUser())) throw new AccessDeniedException(["Article is not owned by you!"]);
        }

        return $entityB;
    }



    /**
     * @param $object
     * @param array $options
     * @throws InvalidParamsException
     */
    public function validate($object,array $options=[]){
        if (null !== $this->validator && (!isset($options['validate']) || $options['validate'])) {
            $validationErrors = $this->validator->validate($object, null, $this->getGroups());
            if (count($validationErrors) > 0) {
                throw new InvalidParamsException($validationErrors);
            }
        }
    }

    /**
     * @param $data
     * @param ParamConverter $configuration
     * @param Context $context
     * @return bool|mixed
     */
    public function deserialize($data,ParamConverter $configuration,Context $context){
        try {
            $object = $this->serializer->deserialize(
                $data,
                $configuration->getClass(),
                $this->format,
                $context
            );

        } catch (UnsupportedFormatException $e) {
            return $this->throwException(new UnsupportedMediaTypeHttpException($e->getMessage(), $e), $configuration);
        } catch (JMSSerializerException $e) {
            return $this->throwException(new BadRequestHttpException($e->getMessage(), $e), $configuration);
        } catch (SymfonySerializerException $e) {
            return $this->throwException(new BadRequestHttpException($e->getMessage(), $e), $configuration);
        }
        $this->object=$object;
        $this->class=get_class($object);

        return $this->object;
    }

    /**
     * @param $data
     * @param Context $context
     * @return string
     */
    public function serialize($data,Context $context){
        return $this->serializer->serialize($data,$this->format,$context);
    }


    /**
     * @return EntityInterface
     */
    private function getDefaultEntity(): EntityInterface {
        if(!$this->defaultEntity) {
            $this->defaultEntity=$this->serializer->deserialize(
                "{}",
                $this->class,
                $this->format,
                new Context()
            );
        }


        return $this->defaultEntity;
    }

    /**
     * @param array $options
     * @return array
     */
    private function getValidatorOptions(array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'groups' => null,
            'traverse' => false,
            'deep' => false,
        ]);

        return $resolver->resolve(isset($options['validator']) ? $options['validator'] : []);
    }

    public function setValidationGroups(array $options){
        $options = $this->getValidatorOptions($options);
        $this->groups = @$options["groups"]? $options["groups"] : [];
    }

    private function getGroups(){
        return $this->groups;
    }

    /**
     * @param array $array
     * @return mixed
     */
    public function json_encode(array $array){
        return str_replace(['"{','}"','\"'],["{","}",'"'],json_encode($array));
    }

    /**
     * @param \Exception $exception
     * @param ParamConverter $configuration
     * @return bool
     * @throws \Exception
     */
    private function throwException(\Exception $exception, ParamConverter $configuration)
    {
        if ($configuration->isOptional()) {
            return false;
        }

        throw $exception;
    }

    /**
     *
     */
    public function setXml(){
        $this->format="xml";
    }

    /**
     *
     */
    public function setJson(){
        $this->format="json";
    }

    /**
     * @return User
     */
    private function getUser(){
        return $this->userService->getUser();
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }



}