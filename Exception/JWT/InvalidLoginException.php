<?php
namespace Aviatoo\Rest\Exception\JWT;
use Aviatoo\Rest\Exception\Base\ApiException;

/**
 * Class InvalidLoginException
 * @package Aviatoo\Rest\Exception\JWT
 */
class InvalidLoginException extends ApiException
{
    const MESSAGE = 'Invalid login parameters';
    const STATUS_CODE = 401;

    /**
     * InvalidLoginException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
