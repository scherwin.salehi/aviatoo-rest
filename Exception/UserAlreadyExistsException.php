<?php
namespace Aviatoo\Rest\Exception;

use Aviatoo\Rest\Exception\Base\ApiException;

/**
 * Class UserAlreadyExistsException
 * @package Aviatoo\Rest\Exception
 */
class UserAlreadyExistsException extends ApiException
{
    const MESSAGE = 'User already exists!';
    const STATUS_CODE = 400;

    /**
     * UserAlreadyExistsException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [], self::MESSAGE);
    }
}
