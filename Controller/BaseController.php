<?php
namespace Aviatoo\Rest\Controller;

use Aviatoo\Rest\Constants\GroupConstants;
use Aviatoo\Rest\Entity\Interfaces\EntityInterface;
use Aviatoo\Rest\Exception\EntityNotFoundException;
use Aviatoo\Rest\Repository\Pagination;
use Aviatoo\Rest\Service\DoctrineApiManager;
use Aviatoo\Rest\Service\HashGenerator;
use Aviatoo\Rest\Service\ParamConverter\ParameterBuilder;
use Aviatoo\Rest\Service\Response\ControllerResponseFactory;
use Aviatoo\Rest\Service\Response\ResponseFactory;
use Aviatoo\Rest\Service\User\UserService;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class BaseController
 * @package Aviatoo\Rest\Controller
 */
abstract class BaseController extends FOSRestController
{

    /**
     * @param EntityInterface $entity
     */
    protected function save($entity){
        $manager=$this->getManager();
        $manager->persist($entity);
        $manager->flush();
    }

    /**
     * @param EntityInterface $entity
     */
    protected function delete(EntityInterface $entity){
        $this->getManager()->remove($entity);
        $this->getManager()->flush();
    }

    /**
     * @param mixed $data
     * @param array $serialization
     * @param string $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function sendResponseWithData($data, string $message,array $serialization=null)
    {
        if($serialization===null) $serialization=[GroupConstants::ENTITY_OUT];

        $pages = Pagination::getPages();
        if(is_numeric($pages)) $data["total_pages"]=$pages;

        $response = $this->getControllerResponseFactory()->createWithData($data, $serialization, $message);

        return $this->getResponseFactory()->createFromControllerResponse($response);
    }

    /**
     * @return ResponseFactory
     */
    protected function getResponseFactory(): ResponseFactory
    {
        return $this->get(ResponseFactory::class);
    }

    /**
     * @return ControllerResponseFactory
     */
    protected function getControllerResponseFactory(): ControllerResponseFactory
    {
        return $this->get(ControllerResponseFactory::class);
    }

    /**
     * @return DoctrineApiManager
     */
    protected function getManager()
    {
        return $this->get(DoctrineApiManager::class);
    }

    /**
     * @return UserService
     */
    protected function getUserService(){
        return $this->get(UserService::class);

    }

    /**
     * @return HashGenerator
     */
    protected function getHashGenerator(){
        return $this->get(HashGenerator::class);
    }

    protected function getParameterBuilder(){
        return $this->get(ParameterBuilder::class);
    }

    /**
     * @param $class
     * @param $id
     * @throws EntityNotFoundException
     */
    protected function throwEntityNotFound($class,$id){
        $location = new $class();
        $location->setId($id);
        throw new EntityNotFoundException($location);
    }
}
